﻿import { container } from 'shiva';
import { css } from 'emotion';

const title = container({
    className: css({
        fontFamily: '"Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif, -apple-system, BlinkMacSystemFont',
        color: '#6d89bf',
        fontSize: '1.5rem',
        fontWeight: 'bold',
        margin: '1rem',
        border: '1px solid lightgrey',
        borderRadius: '0.5rem',
        padding: '1rem',
        backgroundColor: '#f2f0ff',
        boxShadow: '0 0 8px 0px #d6d6d6',
        minHeight: '20rem'
    }),
    children: 'component-content'
});

export const app = () => {
    if(document.querySelector('#shell')) {
        window['component-content'] = container({
            children: title
        });
    } else {
        container({
            root: true,
            children: title
        })
    }
};

app();