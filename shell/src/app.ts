﻿import { container, useState } from 'shiva';

const [content, setContent] = useState('');
const [header, setHeader] = useState('');

const components = [{
    host: 'http://localhost:1003',
    manifest: '/parcel-manifest.json',
    name: 'component-content',
    update: setContent
}, {
    host: 'http://localhost:1002',
    manifest: '/parcel-manifest.json',
    name: 'component-header',
    update: setHeader
}];

const root = container({
    root: true,
    id: 'shell',
    children: [
        container({
            id: 'component-header',
            children: header
        }),
        container({
            id: 'component-content',
            children: content
        })]
});

const loadComponent = async ({ host, manifest, name, update }: { host: string, manifest: string, name: string, update: (component) => void }) => {
    const response = await fetch(`${host}${manifest}`)
    const data = await response.json();
    const script = document.createElement('script');
    script.id = name;
    // script.crossOrigin = '';
    script.src = `${host}${data['../src/app.ts']}`;
    script.onload = res => {
        update(window[name] as any);
    }
    document.head.appendChild(script);
};

const app = () => {
    components.forEach(component => loadComponent(component));
};

app();
