# Micro-front-ends 🐁
Runtime loading of components!

This allows for teams to deploy to production their own components without being coupled to a build.

Uses script tag injection and a manifest per component for cache busting and quick viewing of updates.

## Install
```
npm run setup
```

## Usage
```
npm start
```

This will start:  
- server at `localhost:1001` representing the shell  
- server at `localhost:1002` representing the header component  
- server at `localhost:1003` representing the content component  

It also starts `parcel` bundler for the shell and the components in watch mode so that changes are automatically compiled.

Component changes will not hot reload the shell so will need a refresh to see changes. Changes to the shell will hot reload.

It is possible to work on the components on their own. `cd` into the folder and `npm start` there.